//=============================================================================
// FirebaseConnection.js
//=============================================================================

/*:
 * @plugindesc Connects to the firebase.
 * @author Rodolfo Jose Bagay
 *
 * @param apiKey
 * @desc Firebase apiKey.
 * @default ?
 *
 * @param authDomain
 * @desc Firebase authDomain.
 * @default ?
 *
 * @param databaseURL
 * @desc Firebase databaseURL.
 * @default ?
 *
 * @param projectId
 * @desc Firebase projectId.
 * @default ?
 *
 * @param storageBucket
 * @desc Firebase storageBucket.
 * @default ?
 * 
 * @param messagingSenderId
 * @desc Firebase messagingSenderId.
 * @default ?
 * 
 * @param spefNode
 * @desc specificNode on firebase.
 * @default ?
 * 
 * @param connectOnStart
 * @desc Yes/No.
 * @default Yes
 * 
 * @param isOverRideLogin
 * @desc Yes/No.
 * @default No
 * 
 * @param overRideEmail
 * @desc email format must posses \@ character.
 * @default userX@test.com
 * 
 * @help
 *
 * Plugin Command:
 *   firebase init                                   
 *       # This will Initilize the database. (In case you need it manual)
 *
 *   firebase try                                     
 *       # This will Insert testdata on the database.
 *
 *   firebase update json users {id:1, name:"test"}   
 *       # This will update the users node with values of name and id 
 *
 *   firebase update func users userProfile           
 *       # This will update the users node using function userProfile, function must retrun valid json statment.
 *
 *   firebase update var users currentProfile         
 *       # This will update the users node using variable currentProfile, variable must be a valid json statment.
 * Plugin Command Shortcuts:
 *   _$f i                                   
 *       # This will Initilize the database. (In case you need it manual)
 *
 *   _$f t                                     
 *       # This will Insert testdata on the database.
 *
 *   _$f u j users {id:1, name:"test"}   
 *       # This will update the users node with values of name and id 
 *
 *   _$f u f users userProfile           
 *       # This will update the users node using function userProfile, function must retrun valid json statment.
 *
 *   _$f u v users currentProfile         
 *       # This will update the users node using variable currentProfile, variable must be a valid json statment.
 */

var _$f = "Connecting to Server....";

(function() {

    var parameters = PluginManager.parameters('FirebaseConnection');
    var config = {
        apiKey: String(parameters['apiKey'] || '?'),
        authDomain: String(parameters['authDomain'] || '?'),
        databaseURL: String(parameters['databaseURL'] || '?'),
        projectId: String(parameters['projectId'] || '?'),
        storageBucket: String(parameters['storageBucket'] || '?'),
        messagingSenderId: String(parameters['messagingSenderId'] || '?')
    };
    var spefNode;
    if (String(parameters['spefNode'] || '?') == "?") {
        spefNode = "";
    } else {
        spefNode = String(parameters['spefNode'] || '?') + "/";
    }
    var isInit = false;
    var MainApp;
    if (String(parameters['connectOnStart'] || '?') === "Yes") {
        MainApp = firebase.initializeApp(config);
        _$f = "Done!";
    }

    var _Game_Interpreter_pluginCommand = Game_Interpreter.prototype.pluginCommand;
    Game_Interpreter.prototype.pluginCommand = function(command, args) {
        _Game_Interpreter_pluginCommand.call(this, command, args);
        if (command === 'firebase') {
            switch (args[0]) {
                case 'init':
                    $gameSystem.initConnection();
                    break;
                case 'try':
                    $gameSystem.testConnection();
                    break;
                case 'auth':
                    $gameSystem.authConnection(args[1], args[2]);
                case 'update':
                    $gameSystem.updateData(args[2], args[3], args[1]);
                case 'regread':
                    $gameSystem.registerReadFunction(args[1], args[2]);
                case 'console':
                    console.log($gameMap);
                    break;
            }
        } else if (command === "_$f") {
            switch (args[0]) {
                case 'i':
                    $gameSystem.initConnection();
                    break;
                case 't':
                    $gameSystem.testConnection();
                    break;
                case 'a':
                    $gameSystem.authConnection(args[1], args[2]);
                case 'u':
                    $gameSystem.updateData(args[2], args[3], args[1]);
                case 'c':
                    console.log($gameMap);
                    break;
            }
        }
    };
    //-----------------------------------------------------------------------
    Game_System.prototype.initConnection = function() {
        //SceneManager.push(Scene_ItemBook);
        if (MainApp) {
            console.log("Already initializeApp");
        } else {
            MainApp = firebase.initializeApp(config);

        }

    };
    //-----------------------------------------------------------------------
    Game_System.prototype.authConnection = function(e, p,cl,er) {

        firebase.auth().signInWithEmailAndPassword(e, p).then(function() {
            cl();
        }).catch(function(error){
            er(error);
        });
    };
    //-----------------------------------------------------------------------
    Game_System.prototype.testConnection = function() {
        MainApp.database().ref(spefNode + 'test/').set({
            test1: "A",
            test2: "B",
            test3: {
                A: 1,
                B: 2,
                c: {
                    x1: "A",
                    x2: "B"
                }
            }
        });
    };
    //-----------------------------------------------------------------------
    Game_System.prototype.updateData = function(n, d, m) {
        if (m === "json" || m === "j") {
            MainApp.database().ref(spefNode + n).set(d);
        } else if (m === "func" || m === "f") {
            MainApp.database().ref(spefNode + n).set(eval(d));
        } else if (m === "var" || m === "v") {
            MainApp.database().ref(spefNode + n).set($gameVariables.value[d]);
        } 
    };
    //-----------------------------------------------------------------------
    //$gameVariables.value(x) to read and $gameVariables.setValue(x, value) 
    Game_System.prototype.readData = function(n, v, cl) {
        MainApp.database().ref(spefNode + n).once("value").then(function(snap) {
            var value = snap.val();
            if(v){
                $gameVariables.setValue(v, JSON.stringify(value)) ;
            }
            if (cl) {
                cl(value);
            }
        });
    }
    Game_System.prototype.registerReadFunction = function(n, v, cl) {
        MainApp.database().ref(spefNode + n).on("value", function(snap) {
            var value = snap.val();
            if(v){
                $gameVariables.setValue(v, JSON.stringify(value)) ;
            }
            if (cl) {
                cl(value);
            }

        });
    }
    //-----------------------------------------------------------------------
    Game_System.prototype.deleteData = function() {
        MainApp.database().ref(spefNode + n).set(null);
    };

    Game_System.prototype.CheckConnection = function() {
        MainApp.database().ref(".info/connected").on("value", function(snap) {
            if (snap.val() === true) {
                _$f = 1;
            } else {
                _$f = 0;
            }
        });
    };


})();