//=============================================================================
// playerAction.js
//=============================================================================

/*:
 * @plugindesc ActionEngine to firebase.
 * @author Rodolfo Jose Bagay
 *
 * @param startingMapId
 * @desc Specify the commond event id.
 * @default 1
 *
 * @param startingX
 * @desc Specify the commond event id.
 * @default 0
 *
 * @param startingY
 * @desc Specify the commond event id.
 * @default 0
 *
 * @help
 *
 */

var playerAction;
(function() {
    var parameters = PluginManager.parameters('playerAction');
    playerAction = {
        getPlayerPosition: function() {
            var x = {
                _x: $gamePlayer._x,
                _y: $gamePlayer._y,
                _characterName : $gamePlayer._characterName,
                _mapId : $gameMap._mapId,
                _characterIndex : $gamePlayer._characterIndex
            }
            return x;
        },
        telePlayer: function(x, y, id) {
            $gamePlayer.reserveTransfer(1, 4, 4, 0, 0);
        }
    };
    playerAction.lastPosition = {};
    playerAction.Player_Action_Walk = Game_Actor.prototype.onPlayerWalk;
    Game_Actor.prototype.onPlayerWalk = function() {
        playerAction.Player_Action_Walk.call(this);

        if (playerAction.lastPosition != playerAction.getPlayerPosition()) {
            playerAction.lastPosition = playerAction.getPlayerPosition();
            updateLocation();
        }
    };

    //-----------------------------------------------------------------------
    Game_Player.prototype.performTransfer = function() {
        if (this.isTransferring()) {
            this.setDirection(this._newDirection);
            if (this._newMapId !== $gameMap.mapId() || this._needsMapReload) {
                $gameMap.setup(this._newMapId);
                this._needsMapReload = false;
            }
            this.locate(this._newX, this._newY);
            this.refresh();
            this.clearTransferInfo();
            $gameMap.onlinePlayers = {};
            
            setTimeout(function() {
                $gameSystem.registerReadFunction("playerOnMaps/" + $gameMap._mapId + "/", null, function(playersX) {
                    $.each(playersX, function(id, _ai_loc) {
                        
                        if (_$us != id) {
                            if (!$gameMap.onlinePlayers[id]) {
                                $gameMap.spawnEvent(18, _ai_loc._x,_ai_loc._y);
                                $gameMap.onlinePlayers[id] = $gameMap._lastSpawnEventId;
                                if(_ai_loc._characterName){
                                     $gameMap._events[$gameMap._lastSpawnEventId]._characterName = _ai_loc._characterName;
                                }
                                if(_ai_loc._characterIndex){
                                    $gameMap._events[$gameMap._lastSpawnEventId]._characterIndex = _ai_loc._characterIndex;
                                }
                               
                            }
                            //console.log("gen");
                            
                            var _tempId = $gameMap.onlinePlayers[id];
                            var _c_locX = $gameMap._events[_tempId]._x - _ai_loc._x;
                            var _c_locY = $gameMap._events[_tempId]._y - _ai_loc._y;
                            if (_c_locX == 0 && _c_locY == 0) {

                            } else if (_c_locX >= 1 && _c_locX <= 5) {
                                $gameMap._events[_tempId]._direction = 4;
                                $gameMap._events[_tempId]._x = _ai_loc._x;
                            } else if (_c_locX <= -1 && _c_locX >= -5) {
                                $gameMap._events[_tempId]._direction = 6;
                                $gameMap._events[_tempId]._x = _ai_loc._x;
                            } else if (_c_locY >= 1 && _c_locY <= 5) {
                                $gameMap._events[_tempId]._direction = 8;
                                $gameMap._events[_tempId]._y = _ai_loc._y;
                            } else if (_c_locY <= -1 && _c_locY >= -5) {
                                $gameMap._events[_tempId]._direction = 2;
                                $gameMap._events[_tempId]._y = _ai_loc._y;
                            } else {
                                $gameMap._events[_tempId].teleportToPoint(_ai_loc._x, _ai_loc._y);
                            }
                            

                        }
                        
                    });

                });
            }, 100);

        }
    };

    var __onStart = false;
    var __tempx = Scene_Map.prototype.start;
    Scene_Map.prototype.start = function() {
        __tempx.call(this);
        $gameSystem.CheckConnection();

        if (__onStart === false) {
            __onStart = true;
            $gameSystem.readData("users/" + _$us + "/_actors/", null, function(_actors) {
                if (_actors) {
                    $gameParty._actors = _actors;
                } else {
                    updatePartyData();
                }

                $gameSystem.readData("users/" + _$us + "/lastloc/", null, function(_loc) {
                    if (_loc) {
                        $gamePlayer.reserveTransfer(_loc._mapId, _loc._x, _loc._y, 0, 0);
                    }
                });
            });


        }
        this._myWindow = new MyWindow(5, 5);
        this.addWindow(this._myWindow);
    };

    function getInitPlayerOnCurrentMap(m) {
        var _mapId;
        if (m) {
            _mapId = m;
        } else {
            _mapId = $gameMap._mapId;
        }
        $gameSystem.readData("playerOnMaps/" + _mapId + "/", null, function(value) {

        });
    };

    function updatePartyData() {
        $gameSystem.updateData("users/" + _$us + "/_gold/", $gameParty._gold, "j");
        $gameSystem.updateData("users/" + _$us + "/_actors/", $gameParty._actors, "j");
    }


    var Scene_Menu_onFormationOk = Scene_Menu.prototype.onFormationOk;
    Scene_Menu.prototype.onFormationOk = function() {
        Scene_Menu_onFormationOk.call(this);
        $gameSystem.updateData("users/" + _$us + "/_actors/", $gameParty._actors, "j");
    };

    function updateLocation() {
        $gameSystem.updateData("users/" + _$us + "/lastloc/", "playerAction.getPlayerPosition()", "f");
        $gameSystem.updateData("playerOnMaps/" + $gameMap._mapId + "/" + _$us + "/", "playerAction.getPlayerPosition()", "f");
    }


    var __updatex = Scene_Map.prototype.update;
    Scene_Map.prototype.update = function() {
        __updatex.call(this);
        this._myWindow.refresh();
    }

    function MyWindow() {
        this.initialize.apply(this, arguments);
    };

    MyWindow.prototype = Object.create(Window_Base.prototype);
    MyWindow.prototype.constructor = MyWindow;

    MyWindow.prototype.initialize = function(x, y) {
        Window_Base.prototype.initialize.call(this, x, y, this.windowX(), this.windowY());
        this.status = "x";
        this.refresh();
    };
    MyWindow.prototype.refresh = function() {
        if (this.status != _$f) {
            if (_$f == 1) {
                this.status = "Connected";
            } else {
                this.status = "Connecting....";
            }
            this.contents.clear();
            this.drawText(this.status, 0, 0, this.windowX(), 'left');
        }

    };

    MyWindow.prototype.windowX = function() {
        return 500;
    };
    MyWindow.prototype.windowY = function() {
        return 75;
    };


})();